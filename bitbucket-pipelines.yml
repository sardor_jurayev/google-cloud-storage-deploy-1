image:
  name: atlassian/default-image:2


setup: &setup
  step:
    name: Setup test storage
    image: google/cloud-sdk:286.0.0-slim
    script:
      - export DEPLOYMENT_NAME="google-cloud-storage-deployment-${BITBUCKET_BUILD_NUMBER}"
      - export STORAGE_NAME="gcp-storage-deploy-pipes-test-${BITBUCKET_BUILD_NUMBER}"
      - export STORAGE_REGION="us-central1"
      - echo "${GCP_KEY_FILE}" | base64 -d >> /tmp/key-file.json
      - gcloud config set project ${PROJECT}
      - gcloud auth activate-service-account --key-file=/tmp/key-file.json
      - gcloud deployment-manager deployments create ${DEPLOYMENT_NAME} --template test/storagebucket-config.py --properties=STORAGE_REGION:${STORAGE_REGION},STORAGE_NAME:${STORAGE_NAME}


test: &test
  parallel:
    - step:
        name: Build and Test
        image: google/cloud-sdk:324.0.0-slim
        script:
        - export STORAGE_NAME="gcp-storage-deploy-pipes-test-${BITBUCKET_BUILD_NUMBER}"
        - export STORAGE_REGION="us-central1"
        - export DEPLOYMENT_NAME="google-cloud-storage-deployment-${BITBUCKET_BUILD_NUMBER}"
        - echo "${GCP_KEY_FILE}" | base64 -d >> /tmp/key-file.json
        - export GOOGLE_APPLICATION_CREDENTIALS="/tmp/key-file.json"
        - pip3 install -r test/requirements.txt
        - flake8 --ignore E501,E125,F541
        - pytest test/test.py
        after-script:
        - gcloud config set project $PROJECT
        - gcloud auth activate-service-account --key-file=/tmp/key-file.json
        - gsutil -m rm gs://gcp-storage-deploy-pipes-test-*/**.txt
        - gsutil rm -r gs://gcp-storage-deploy-pipes-test-*
        - yes | gcloud deployment-manager deployments delete ${DEPLOYMENT_NAME}
        services:
        - docker
    - step:
        name: Lint the Dockerfile
        image: hadolint/hadolint:latest-debian
        script:
          - hadolint Dockerfile


release-dev: &release-dev
  step:
    name: Release development version
    trigger: manual
    image: python:3.7
    script:
      - pip install semversioner
      - VERSION=$(semversioner current-version).${BITBUCKET_BUILD_NUMBER}-dev
      - pipe: docker://bitbucketpipelines/bitbucket-pipe-release:3.2.2
        variables:
          DOCKERHUB_USERNAME: $DOCKERHUB_USERNAME
          DOCKERHUB_PASSWORD: $DOCKERHUB_PASSWORD
          IMAGE: bitbucketpipelines/$BITBUCKET_REPO_SLUG
          GIT_PUSH: 'false'
          VERSION: ${VERSION}
    services:
      - docker


push: &push
  step:
    name: Push and Tag
    image: python:3.7
    script:
      - pipe: docker://bitbucketpipelines/bitbucket-pipe-release:3.2.2
        variables:
          DOCKERHUB_USERNAME: $DOCKERHUB_USERNAME
          DOCKERHUB_PASSWORD: $DOCKERHUB_PASSWORD
          IMAGE: bitbucketpipelines/$BITBUCKET_REPO_SLUG
    services:
      - docker


pipelines:
  default:
  - <<: *setup
  - <<: *test
  - <<: *release-dev
  branches:
    master:
    - <<: *setup
    - <<: *test
    - <<: *push
  custom:
    test-and-report:
    - step:
        script:
        - apt-get update && apt-get install -y bats
        - export DOCKERHUB_IMAGE="bitbucketpipelines/${BITBUCKET_REPO_SLUG}"
        - export DOCKERHUB_TAG=${BITBUCKET_BUILD_NUMBER}
        - echo "Building image..."
        - docker build -t ${DOCKERHUB_IMAGE}:${DOCKERHUB_TAG} .
        - echo "Testing image..."
        - set +e
        - test/test.bats; test_exit_code=$?
        - set -e
        - echo test_exit_code = $test_exit_code
        - pipe: atlassian/report-task-test-result:1.0.0
          variables:
            DATADOG_API_KEY: $DATADOG_API_KEY
            TASK_NAME: google-cloud-storage-deploy
            TEST_EXIT_CODE: $test_exit_code
        - exit $test_exit_code
        services:
        - docker
