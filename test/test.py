import os
import time
import shutil
from pathlib import Path

from google.cloud import storage

from bitbucket_pipes_toolkit.test import PipeTestCase


PIPES_MESSAGE = "Pipelines is awesome!"


class CloudStorageDeployTestCase(PipeTestCase):
    def setUp(self):
        super().setUp()
        random_number = str(time.time_ns())
        self.local_dir = f"gcp-storage-{random_number}"
        os.mkdir(self.local_dir)

        self.filename = f'deployment-{random_number}.txt'
        self.fullname = os.path.join(self.local_dir, self.filename)

        with open(self.fullname, 'w+') as deployment_file:
            deployment_file.write(PIPES_MESSAGE)

    def tearDown(self):
        shutil.rmtree(self.local_dir, ignore_errors=True)
        shutil.rmtree(f'symlink-{self.local_dir}', ignore_errors=True)

    def test_upload_to_cloud_storage(self):
        result = self.run_container(environment={
            'KEY_FILE': os.getenv("GCP_KEY_FILE"),
            'PROJECT': os.getenv("PROJECT"),
            'BUCKET': os.getenv("STORAGE_NAME"),
            'SOURCE': self.local_dir,
            'CACHE_CONTROL': "max-age: 30",
            'CONTENT_DISPOSITION': "inline",
            'CONTENT_ENCODING': "identity",
            'CONTENT_TYPE': "text/plain",
            'ACL': "project-private",
            'STORAGE_CLASS': "nearline",
        })

        client = storage.Client()
        bucket = client.get_bucket(os.getenv("STORAGE_NAME"))
        blob = bucket.get_blob(self.fullname)

        self.assertRegex(result, rf'✔ Deployment successful')
        self.assertIn(PIPES_MESSAGE, blob.download_as_text())

    def test_upload_with_wildcard_source(self):
        result = self.run_container(environment={
            'KEY_FILE': os.getenv("GCP_KEY_FILE"),
            'PROJECT': os.getenv("PROJECT"),
            'BUCKET': os.getenv("STORAGE_NAME"),
            'SOURCE': f'{self.local_dir}/**',
            'CACHE_CONTROL': "max-age: 30",
            'CONTENT_DISPOSITION': "inline",
            'CONTENT_ENCODING': "identity",
            'CONTENT_TYPE': "text/plain",
            'ACL': "project-private",
            'STORAGE_CLASS': "nearline",
        })

        client = storage.Client()
        bucket = client.get_bucket(os.getenv("STORAGE_NAME"))
        blob = bucket.get_blob(self.filename)

        self.assertRegex(result, rf'✔ Deployment successful')
        self.assertIn(PIPES_MESSAGE, blob.download_as_text())

    def test_upload_one_file(self):
        result = self.run_container(environment={
            'KEY_FILE': os.getenv("GCP_KEY_FILE"),
            'PROJECT': os.getenv("PROJECT"),
            'BUCKET': os.getenv("STORAGE_NAME"),
            'SOURCE': f'{self.fullname}',
            'CACHE_CONTROL': "max-age: 30",
            'CONTENT_DISPOSITION': "inline",
            'CONTENT_ENCODING': "identity",
            'CONTENT_TYPE': "text/plain",
            'ACL': "project-private",
            'STORAGE_CLASS': "nearline",
        })

        client = storage.Client()
        bucket = client.get_bucket(os.getenv("STORAGE_NAME"))
        blob = bucket.get_blob(self.filename)

        self.assertRegex(result, rf'✔ Deployment successful')
        self.assertIn(PIPES_MESSAGE, blob.download_as_text())

    def test_upload_symlinked_file(self):
        symlinked_file = f'symlink-{self.filename}'
        full_symlinked_filename = os.path.join(self.local_dir, symlinked_file)

        Path(os.path.join(self.local_dir, symlinked_file)).symlink_to(Path(self.filename))

        result = self.run_container(environment={
            'KEY_FILE': os.getenv("GCP_KEY_FILE"),
            'PROJECT': os.getenv("PROJECT"),
            'BUCKET': os.getenv("STORAGE_NAME"),
            'SOURCE': full_symlinked_filename,
            'CACHE_CONTROL': "max-age: 30",
            'CONTENT_DISPOSITION': "inline",
            'CONTENT_ENCODING': "identity",
            'CONTENT_TYPE': "text/plain",
            'ACL': "project-private",
            'STORAGE_CLASS': "nearline",
        })

        client = storage.Client()
        bucket = client.get_bucket(os.getenv("STORAGE_NAME"))
        blob = bucket.get_blob(symlinked_file)

        self.assertRegex(result, rf'✔ Deployment successful')
        self.assertIn(PIPES_MESSAGE, blob.download_as_text())

    def test_upload_symlinked_directory(self):
        symlinked_dir = f'symlink-{self.local_dir}'
        Path(symlinked_dir).symlink_to(Path(self.local_dir, target_is_directory=True))

        result = self.run_container(environment={
            'KEY_FILE': os.getenv("GCP_KEY_FILE"),
            'PROJECT': os.getenv("PROJECT"),
            'BUCKET': os.getenv("STORAGE_NAME"),
            'SOURCE': symlinked_dir,
            'CACHE_CONTROL': "max-age: 30",
            'CONTENT_DISPOSITION': "inline",
            'CONTENT_ENCODING': "identity",
            'CONTENT_TYPE': "text/plain",
            'ACL': "project-private",
            'STORAGE_CLASS': "nearline",
        })

        client = storage.Client()
        bucket = client.get_bucket(os.getenv("STORAGE_NAME"))
        blob = bucket.get_blob(f"{symlinked_dir}/{self.filename}")

        self.assertRegex(result, rf'✔ Deployment successful')
        self.assertIn(PIPES_MESSAGE, blob.download_as_text())

    def test_should_fail_if_source_doent_exist(self):
        result = self.run_container(environment={
            'KEY_FILE': os.getenv("GCP_KEY_FILE"),
            'PROJECT': os.getenv("PROJECT"),
            'BUCKET': os.getenv("STORAGE_NAME"),
            'SOURCE': "does-not-exist",
            'CACHE_CONTROL': "max-age: 30",
            'CONTENT_DISPOSITION': "inline",
            'CONTENT_ENCODING': "identity",
            'CONTENT_TYPE': "text/plain",
            'ACL': "public-read",
            'STORAGE_CLASS': "nearline",
        })

        self.assertIn("CommandException: 1 file/object could not be transferred.", result)
